"""
LLDB Boost Formatters

Contains formatters for:
- `boost::container::flat_map` - https://www.boost.org/doc/libs/1_69_0/doc/html/boost/container/flat_map.html
- `boost::optional` - https://www.boost.org/doc/libs/1_69_0/libs/optional/doc/html/index.html

Load into LLDB with `command script import /path/to/extra-formatters.py`
or add this command to `~/.lldbinit`

Links:
- https://lldb.llvm.org/varformats.html
- https://github.com/llvm/llvm-project/tree/master/lldb/examples
- https://github.com/llvm/llvm-project/blob/master/llvm/utils/lldbDataFormatters.py
"""

import lldb


def __lldb_init_module(debugger, internal_dict):
    # lldb.formatters.Logger._lldb_formatters_debug_level = 1
    # lldb.formatters.Logger._lldb_formatters_debug_filename = '/tmp/lldb_formatters.log'

    debugger.HandleCommand('type category define -e boost -l c++')

    debugger.HandleCommand(
        'type synthetic add -w boost -l lldb_boost_formatters.BoostFlatMapSynthProvider -x "^boost::container::flat_map<.+>$"')
    debugger.HandleCommand(
        'type summary add --expand -x "^boost::container::flat_map<.+>$" --summary-string "${svar%#} items')

    debugger.HandleCommand(
        'type synthetic add -w boost -l lldb_boost_formatters.BoostOptionalSynthProvider -x "^boost::optional<.+>$"')
    debugger.HandleCommand(
        'type summary add --inline-children -w boost -x "^boost::optional<.+>$"')

    debugger.HandleCommand(
        'type synthetic add -w boost -l lldb_boost_formatters.BoostVariantSyntheticChildrenProvider -x "^boost::variant<.+>$"')
    debugger.HandleCommand(
        'type summary add --inline-children -w boost -x "^boost::variant<.+>$"')


# TODO: also show comparator
class BoostFlatMapSynthProvider:
    def __init__(self, valobj, dict):
        self.valobj = valobj
        self.update()

    def num_children(self):
        if self.size < 0:
            return 0
        return self.size.GetValueAsUnsigned(0)

    def get_child_index(self, name):
        try:
            return int(name.lstrip('[').rstrip(']'))
        except:
            return -1

    def get_child_at_index(self, index):
        if index < 0 or index >= self.num_children():
            return None

        offset = index * self.type_size
        return self.data.CreateChildAtOffset('['+str(index)+']', offset, self.value_type)

    def update(self):
        boost_vector = self.valobj.GetChildMemberWithName(
            'm_flat_tree').GetChildMemberWithName('m_data').GetChildMemberWithName('m_seq')
        holder = boost_vector.GetChildMemberWithName('m_holder')
        self.data = holder.GetChildMemberWithName('m_start')
        self.size = holder.GetChildMemberWithName('m_size')

        the_type = boost_vector.GetType()
        if the_type.IsReferenceType():
            the_type = the_type.GetDereferencedType()

        self.value_type = the_type.GetTemplateArgumentType(0)
        self.type_size = self.value_type.GetByteSize()


class BoostOptionalSynthProvider:
    def __init__(self, valobj, dict):
        self.valobj = valobj
        self.update()

    def num_children(self):
        return 1

    def get_child_index(self, name):
        try:
            return int(name.lstrip('[').rstrip(']'))
        except:
            return -1

    def get_child_at_index(self, index):
        if index == 0:
            if self.initialized.GetValueAsUnsigned(0) == 1:
                return self.storage.CreateChildAtOffset('value', 0, self.value_type)
            else:
                return self.storage.CreateValueFromExpression('initialised', 'false')

        return None

    def update(self):
        self.initialized = self.valobj.GetChildMemberWithName('m_initialized')

        the_type = self.valobj.GetType()
        if the_type.IsReferenceType():
            the_type = the_type.GetDereferencedType()
        self.value_type = the_type.GetTemplateArgumentType(0)

        self.storage = self.valobj.GetChildMemberWithName(
            'm_storage').GetChildMemberWithName('dummy_')


def OptionalSummaryProvider(valobj, internal_dict):
    storage = valobj.GetChildMemberWithName(
        'm_storage').GetChildMemberWithName('dummy_')
    if not storage:
        storage = valobj

    failure = 2
    initialized = valobj.GetChildMemberWithName(
        'm_initialized').GetValueAsUnsigned(failure)
    if initialized == failure:
        return '<could not read boost::optional>'

    if initialized == 0:
        return 'uninitialised'

    underlying_type = storage.GetType()
    if underlying_type.IsReferenceType():
        underlying_type = underlying_type.GetDereferencedType()
    underlying_type = underlying_type.GetTemplateArgumentType(0)

    # logger = lldb.formatters.Logger.Logger()
    # logger >> "underlying_type " + underlying_type.name

    return str(storage.Cast(underlying_type))


def templateParams(s):
    depth = 0
    start = s.find('<') + 1
    result = []
    for i, c in enumerate(s[start:], start):
        if c == '<':
            depth += 1
        elif c == '>':
            if depth == 0:
                result.append(s[start: i].strip())
                break
            depth -= 1
        elif c == ',' and depth == 0:
            result.append(s[start: i].strip())
            start = i+1
    return result


def BoostVariantSummary(valobj, internal_dict):
    tps = templateParams(valobj.type.GetCanonicalType().name)
    return 'variant<' + tps[valobj.GetChildMemberWithName('which_').signed] + '>'


class BoostVariantSyntheticChildrenProvider:
    nodeNames = ['value']

    def __init__(self, valobj, internal_dict):
        self.valobj = valobj
        self.typeParams = templateParams(valobj.type.GetCanonicalType().name)

    def num_children(self):
        return len(self.nodeNames)

    def has_children(self):
        return True

    def get_child_index(self, name):
        try:
            return self.nodeNames.index(name)
        except ValueError:
            return None

    def get_child_at_index(self, index):
        # logger = lldb.formatters.Logger.Logger()
        # logger >> "Retrieving child " + str(index)

        try:
            node = self.nodeNames[index]
        except IndexError:
            return None
        which_ = self.valobj.GetChildMemberWithName('which_').signed

        # logger >> "which " + str(which_)

        try:
            valueTypeName = self.typeParams[which_]
        except IndexError:
            return self.valobj.CreateValueFromExpression(node, '(const char*)"unknown"')

        # logger >> "type " + valueTypeName

        if node == 'type':
            if valueTypeName.startswith("boost::recursive_wrapper"):
                valueTypeName = templateParams(valueTypeName)[0]
            return self.valobj.CreateValueFromExpression(node, '(const char*)"' + valueTypeName + '"')
        if node == 'value':
            target = lldb.debugger.GetSelectedTarget()
            valueType = target.FindFirstType(valueTypeName)
            value = self.valobj.GetChildMemberWithName(
                'storage_').Cast(valueType)
            if valueTypeName.startswith("boost::recursive_wrapper"):
                value = value.GetChildMemberWithName('p_').Dereference()
                valueType = value.type

            return self.valobj.CreateValueFromData(node, value.GetData(), valueType)
        return None

    def update(self):
        pass
