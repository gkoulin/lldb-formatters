"""
LLDB Boost Formatters

Contains formatters for:
- `boost::container::flat_map` - https://www.boost.org/doc/libs/1_69_0/doc/html/boost/container/flat_map.html
- `boost::optional` - https://www.boost.org/doc/libs/1_69_0/libs/optional/doc/html/index.html

Load into LLDB with `command script import /path/to/extra-formatters.py`
or add this command to `~/.lldbinit`

Links:
- https://lldb.llvm.org/varformats.html
- https://github.com/llvm/llvm-project/tree/master/lldb/examples
- https://github.com/llvm/llvm-project/blob/master/llvm/utils/lldbDataFormatters.py
"""

import lldb


def __lldb_init_module(debugger, internal_dict):
    # lldb.formatters.Logger._lldb_formatters_debug_level = 1
    # lldb.formatters.Logger._lldb_formatters_debug_filename = '/tmp/lldb_formatters.log'

    debugger.HandleCommand('type category define -e std -l c++')

    debugger.HandleCommand(
        'type synthetic add -w std -l lldb_std_formatters.StdSetSyntheticChildProvider -x "^std::set<.+>$"')
    debugger.HandleCommand(
        'type summary add -w std -F lldb_std_formatters.StdSetSummaryProvide -x "^std::set<.+>$"')


def callMethod(valobj, expr):
    frame = valobj.GetFrame()
    name = valobj.GetName()
    return frame.EvaluateExpression(name + '.' + expr)


def getStdSetValueType(valobj):
    set_type = valobj.GetType()
    if set_type.IsReferenceType():
        set_type = set_type.GetDereferencedType()
    return set_type.GetTemplateArgumentType(0)


class StdSetSyntheticChildProvider:
    def __init__(self, valobj, dict):
        self.valobj = valobj
        self.update()

    def num_children(self):
        return self.size

    def get_child_index(self, name):
        try:
            return int(name.lstrip('[').rstrip(']'))
        except:
            return -1

    def get_child_at_index(self, index):
        if index < 0 or index >= self.num_children():
            return None

        name = self.valobj.GetName()
        expr = name + '.begin()'
        for i in range(0, index):
            expr = '++' + expr
        expr = '*(' + expr + ')'

        frame = self.valobj.GetFrame()
        iterator = frame.EvaluateExpression(expr)

        # logger = lldb.formatters.Logger.Logger()
        # logger >> "StdSetProvider::iterator index: " + str(index)
        # logger >> "StdSetProvider::iterator expr: " + expr
        # logger >> "StdSetProvider::value_type: " + iterator.GetTypeName()
        # logger >> "StdSetProvider::data: " + str(iterator.GetData())

        return iterator.CreateValueFromData('['+str(index)+']', iterator.GetData(), self.value_type)

    def update(self):
        self.size = callMethod(self.valobj, 'size()').GetValueAsUnsigned(0)
        self.value_type = getStdSetValueType(self.valobj)

        # logger = lldb.formatters.Logger.Logger()
        # logger >> "StdSetProvider::size: " + str(self.size)
        # logger >> "StdSetProvider::value_type: " + self.value_type.GetName()


def StdSetSummaryProvide(valobj, internal_dict, options):
    size = callMethod(valobj, 'size()').GetValueAsUnsigned(0)
    return '{ size: ' + str(size) + ' }'
